FROM maven:3.9.5-eclipse-temurin-17-alpine
LABEL authors="w491h"

# udpate
RUN apk update
RUN apk upgrade
# Tree For debugging

RUN apk fetch tree
RUN mkdir /etc/maven
ADD settings.xml /etc/maven